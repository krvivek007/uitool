FROM jetty
MAINTAINER vivek.kumar@akqa.com
ADD ./target/*.war /var/lib/jetty/webapps/root.war

WORKDIR /var/lib/jetty
CMD ["java","-jar","/usr/local/jetty/start.jar"]
