package com.akqa.tools.persistence;

import lombok.Getter;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

public enum MyHazleCastInstance {

	INSTANCE();
	
	@Getter private  HazelcastInstance instance;

	MyHazleCastInstance(){
		Config cfg = new Config();
		instance = Hazelcast.newHazelcastInstance(cfg);
	}
}
