package com.akqa.tools.persistence;

import java.util.Map;

import com.akqa.tools.network.model.Result;

public interface PersistenceService {
	Map<Integer, Result> getResults(String key);
	void clean(String key);
}
