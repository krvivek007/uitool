package com.akqa.tools.persistence;

import java.util.Map;

import com.akqa.tools.network.model.Result;

public class HazelCastPersistenceServiceImpl implements PersistenceService{

	@Override
	public Map<Integer, Result> getResults(String key) {
		return MyHazleCastInstance.INSTANCE.getInstance().getMap(key);
	}

	@Override
	public void clean(String key) {
		MyHazleCastInstance.INSTANCE.getInstance().getMap(key).destroy();
	}
	
	
}
