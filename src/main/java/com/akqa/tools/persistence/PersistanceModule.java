package com.akqa.tools.persistence;

import com.google.inject.AbstractModule;

public class PersistanceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(PersistenceService.class).to(HazelCastPersistenceServiceImpl.class);
	}
}
