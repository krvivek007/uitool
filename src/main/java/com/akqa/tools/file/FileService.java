package com.akqa.tools.file;

import java.util.List;

import com.akqa.tools.network.model.Result;

public interface FileService {
	List<Result> getResult(String fileName);
}
