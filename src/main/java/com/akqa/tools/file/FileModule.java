package com.akqa.tools.file;

import com.google.inject.AbstractModule;

public class FileModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(FileService.class).to(FIleServiceImpl.class);
	}
}
