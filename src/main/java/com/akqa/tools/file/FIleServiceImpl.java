package com.akqa.tools.file;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.SneakyThrows;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.akqa.tools.network.CommonUtils;
import com.akqa.tools.network.model.Result;
import com.google.inject.Singleton;

@Singleton
public class FIleServiceImpl implements FileService{

	@Override
	@SneakyThrows
	public List<Result> getResult(String fileName) {
		List<Result> finalResult = new ArrayList<Result>();
		try{
			FileInputStream fileIs = new FileInputStream(new File(fileName));
			HSSFWorkbook workbook = new HSSFWorkbook(fileIs);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Stream<Row> rowStream = CommonUtils.iteratorToStream(sheet.iterator());
			finalResult.addAll(rowStream.parallel().map(row -> mapRow(row)).filter(FIleServiceImpl::valid).collect(Collectors.toList()));
			workbook.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return finalResult;

	}
	private  Result mapRow(Row row){
		Result result = new Result();
		result.setRowNum(row.getRowNum());
		CommonUtils.iteratorToStream(
				row.cellIterator()).forEach(aCell ->result.addCoumn(CellReference.convertNumToColString(aCell.getColumnIndex()), getValue(aCell)));
		return result;
	}
	
	private static boolean valid(Result result){
		return InetAddressValidator.getInstance().isValidInet4Address(result.getIp());
	}

	@SuppressWarnings("deprecation")
	private static String getValue(Cell cell){
		switch(cell.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN:
			return String.valueOf(cell.getBooleanCellValue());
		case Cell.CELL_TYPE_NUMERIC:
			return String.valueOf(cell.getNumericCellValue());
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
		}
		return "";
	}

}
