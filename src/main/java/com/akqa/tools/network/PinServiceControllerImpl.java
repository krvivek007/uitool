package com.akqa.tools.network;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.inject.Named;

import lombok.Getter;
import lombok.SneakyThrows;

import com.akqa.tools.network.checker.NetworkChecker;
import com.akqa.tools.network.checker.NewtorkStatusInfo;
import com.akqa.tools.network.checker.PingChecker;
import com.akqa.tools.network.model.Result;
import com.akqa.tools.persistence.PersistenceService;
import com.google.inject.Inject;

public class PinServiceControllerImpl {
	private final List<NetworkChecker> checkers = new ArrayList<NetworkChecker>();
	@Getter private AtomicInteger totalCount; 
	@Getter private AtomicInteger currentCount;
	@Getter private List<Result> finalResult;

	@Inject PersistenceService persistcneService;
	@Inject @Named("SessionID") String sessionId;
	
	public PinServiceControllerImpl() {
		checkers.add(new PingChecker());
	}

	@SneakyThrows
	public void doWork(){
		totalCount = new AtomicInteger(1);
		currentCount = new AtomicInteger(0);
		finalResult = new ArrayList<>();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try{
					Map<Integer, Result> rows  = persistcneService.getResults(sessionId);
					totalCount = new AtomicInteger(rows.size());
					finalResult.addAll(rows.values().stream().parallel().map(row -> mapRow(row)).collect(Collectors.toList()));
					rows.clear();
					finalResult.stream().forEach(result -> rows.put(result.getRowNum(), result));
				}catch(Exception ex){
					ex.printStackTrace();
				}finally{

				}
			}
		}).start();;
	}

	private  Result mapRow(Result row){
		NewtorkStatusInfo statusInfo = NewtorkStatusInfo.builder().url(row.getUrl()).ip(row.getIp()).port(row.getPort()).build();
		for(NetworkChecker checker : checkers){
			checker.checkAndNext(statusInfo);
		}
		row.addCoumn("status", statusInfo.getResult());	
		this.currentCount.incrementAndGet();
		
		return row;
	}
}
