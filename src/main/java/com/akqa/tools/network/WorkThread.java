package com.akqa.tools.network;

import lombok.Setter;

import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;

public class WorkThread extends Thread {
    private double current = 0.0;
    @Setter private ProgressBar progress;
    @Setter private Label status;
    @Setter private PinServiceControllerImpl pingController;
   
    
    @Override
    public void run() {
    	pingController.doWork();
    	while (current < 1.0) {
    		current = pingController.getCurrentCount().get()/(double)pingController.getTotalCount().get();
    		try {
    			sleep(50); // Sleep for 50 milliseconds
    		} catch (InterruptedException e) {}
    		// Update the UI thread-safely
    		UI.getCurrent().access(new Runnable() {
    			@Override
    			public void run() {
    				progress.setValue(new Float(current));
    				if (current < 1.0){
    					status.setValue("" + ((int)(current*100)) + "% done");
    				}else{
    					status.setValue("all done");
    					UI.getCurrent().getNavigator().navigateTo("resultview");
    				}
    			}
    		});
    	}
    }
}
