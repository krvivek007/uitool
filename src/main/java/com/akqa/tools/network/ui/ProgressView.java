package com.akqa.tools.network.ui;

import com.akqa.tools.network.PinServiceControllerImpl;
import com.akqa.tools.network.WorkThread;
import com.google.inject.Inject;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ProgressView extends VerticalLayout implements View{
	final HorizontalLayout layouts = new HorizontalLayout();
	final ProgressBar progress = new ProgressBar(new Float(0.0));
	final Label status = new Label("Completed 0%");
	
	@Inject PinServiceControllerImpl pingController;
	@Inject WorkThread workThread;
	
	public ProgressView() {
		Responsive.makeResponsive(this);
		progress.setEnabled(false);
        layouts.addComponent(progress);
        layouts.addComponent(status);
        layouts.setHeightUndefined();
        addComponent(layouts);
        layouts.setComponentAlignment(progress, Alignment.MIDDLE_CENTER);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		setSizeFull();
		setComponentAlignment(layouts, Alignment.MIDDLE_CENTER);
		workThread.setProgress(progress);
		workThread.setStatus(status);
		workThread.setPingController(pingController);
		workThread.start();
        UI.getCurrent().setPollInterval(500);
        progress.setEnabled(true);
        status.setValue("running...");
		setMargin(true);
	}
}
