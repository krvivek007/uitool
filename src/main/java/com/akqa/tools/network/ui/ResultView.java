package com.akqa.tools.network.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collection;

import javax.inject.Named;

import com.akqa.tools.network.model.Result;
import com.akqa.tools.persistence.PersistenceService;
import com.google.inject.Inject;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class ResultView extends VerticalLayout implements View{
	private static final String COL1_RESULT_NET = "net";
	private static final String COL2_RESULT_FQDN = "fqdn";
	private static final String COL3_RESULT_URL = "url";
	private static final String COL4_RESULT_IP = "ip";
	private static final String COL5_RESULT_PORT = "port";
	private static final String COL6_RESULT_STATUS = "status";
 
	@Inject PersistenceService persistenceService;
	@Inject @Named("SessionID") String sessionId;


	Grid grid = new Grid();
	HorizontalLayout buttons = new HorizontalLayout();
	public ResultView() {
		Responsive.makeResponsive(this);
		grid.setColumns(COL1_RESULT_NET, COL2_RESULT_FQDN, COL3_RESULT_URL, COL4_RESULT_IP, COL5_RESULT_PORT, COL6_RESULT_STATUS );
		grid.setSizeFull();
		Button backButton = new Button("Start Again");

		backButton.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				UI.getCurrent().getNavigator().navigateTo("formview");
			}
		});
		buttons.addComponents(backButton);
		Button export = new Button("Export");

		export.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("/download");
			}
		});
		buttons.addComponents(export);
		buttons.setMargin(new MarginInfo(false, true));
		addComponent(buttons);
		buttons.setHeightUndefined();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		setSizeFull();
		setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		addComponents(grid);
		
		final Collection<Result> results = persistenceService.getResults(sessionId).values();
		grid.setContainerDataSource(new BeanItemContainer<>(Result.class, results));
		setExpandRatio(buttons, 0.2f); 
		setExpandRatio(grid, 0.8f); 
		setMargin(true);
		grid.addItemClickListener(eventx ->	{
			NMAPWindow sub = new NMAPWindow(eventx.getItemId());
	        // Add it to the root component
	        UI.getCurrent().addWindow(sub);
		});
	}

	class NMAPWindow extends Window {
	    public NMAPWindow(Object object) {
	    	super("nmap Details");
	    	setHeight("400px");
	    	setWidth("400px");
	        center();
	        VerticalLayout content = new VerticalLayout();
	        content.addComponent(new Label(getClickDetails(object)));
	        content.setMargin(true);
	        setContent(content);
	        setClosable(false);
	        Button ok = new Button("Ok");
	        ok.addClickListener(new ClickListener() {
	            public void buttonClick(ClickEvent event) {
	                close();
	            }
	        });
	        content.addComponent(ok);
	        content.setComponentAlignment(ok, Alignment.BOTTOM_CENTER);
	    }
	}
	
	private String getClickDetails(Object itemId) {
		Result result = (Result)itemId;
		StringBuilder nmapCommand = new StringBuilder("/usr/local/bin/nmap ");
		StringBuilder resultString = new StringBuilder();
		nmapCommand.append("-Pn ");
		nmapCommand.append("-p " + result.getPort() + " ");
		nmapCommand.append("--reason ");
		nmapCommand.append(result.getUrl());
		try {
			Process p = Runtime.getRuntime().exec(new String[]{
		             "sh", "-c",
		             nmapCommand.toString()
		          });
			BufferedReader stdInput = new BufferedReader(new 
					InputStreamReader(p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new 
					InputStreamReader(p.getErrorStream()));
			String s = "";
			while ((s = stdInput.readLine()) != null) {
				resultString.append(s);
				resultString.append("\n");
			}
			resultString.append("______________\n");
			while ((s = stdError.readLine()) != null) {
				resultString.append(s);
				resultString.append("\n");
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return resultString.toString();
	}
}
