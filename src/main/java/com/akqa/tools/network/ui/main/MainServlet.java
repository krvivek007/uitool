package com.akqa.tools.network.ui.main;

import javax.servlet.annotation.WebServlet;

import com.akqa.tools.network.ui.NetworkToolUI;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = {"/ui/*", "/VAADIN/*"}, name = "MainServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = NetworkToolUI.class, productionMode = true)
public  class MainServlet extends VaadinServlet {
}
