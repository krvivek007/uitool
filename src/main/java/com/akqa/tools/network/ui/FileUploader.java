package com.akqa.tools.network.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import lombok.SneakyThrows;

import org.apache.commons.io.FileUtils;

import com.akqa.tools.file.FileService;
import com.akqa.tools.network.model.Result;
import com.akqa.tools.persistence.PersistenceService;
import com.google.inject.Inject;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;


@SuppressWarnings("serial")
class FileUploader implements Receiver, SucceededListener {
	public String fileName;
	
	@Inject PersistenceService service;
	@Inject @Named("SessionID") String sessionId;
	@Inject FileService fileService;
	
	public OutputStream receiveUpload(String filename, String mimeType) {
		FileOutputStream fos = null; // Stream to write to
		try {
			fileName = "/tmp/uploads/" + sessionId;
			fos = new FileOutputStream(fileName);
		} catch (final java.io.FileNotFoundException e) {
			new Notification("Could not open file\n",
					e.getMessage(),
					Notification.Type.ERROR_MESSAGE)
			.show(Page.getCurrent());
			return null;
		}
		return fos;
	}

	@SneakyThrows
	public void uploadSucceeded(SucceededEvent event) {
		List<Result> finalResult = fileService.getResult(fileName);
		if(fileName != null){
			Map<Integer,Result> map = service.getResults(sessionId);
			map.clear();
			finalResult.stream().forEach(x -> map.put(x.getRowNum(), x));
			FileUtils.forceDelete(new File(fileName));;
		}
		UI.getCurrent().getNavigator().navigateTo("progressview");
	}

	public void uploadSFailed(FailedEvent event) {
		System.out.println("file uploaded...");
	}
};