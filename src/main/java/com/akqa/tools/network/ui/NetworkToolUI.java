package com.akqa.tools.network.ui;

import com.akqa.tools.file.FileModule;
import com.akqa.tools.network.CommonUtils;
import com.akqa.tools.network.WorkerModule;
import com.akqa.tools.persistence.PersistanceModule;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("mytheme")
public class NetworkToolUI extends UI {
	Navigator navigator;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		Injector injector = Guice.createInjector(new FileModule(), new PersistanceModule(), new WorkerModule(),
				new AbstractModule() {
			@Override
			protected void configure() {
				bindConstant().annotatedWith(Names.named("SessionID")).to(CommonUtils.getSessionId());
				bind(FileUploader.class);
			}
		});
		FileUploader uploader = injector.getInstance(FileUploader.class);
		FileUploadFormView fileUploaderView = new FileUploadFormView(uploader);
		ProgressView progressView = injector.getInstance(ProgressView.class);
		ResultView resultView = injector.getInstance(ResultView.class);
		navigator = new Navigator(this, this);
		navigator.addView("formview", fileUploaderView);
        navigator.addView("progressview", progressView);
		navigator.addView("resultview", resultView);
		navigator.navigateTo("formview");
	}
}
