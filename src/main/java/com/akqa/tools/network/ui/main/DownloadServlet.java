package com.akqa.tools.network.ui.main;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.akqa.tools.network.model.Result;
import com.akqa.tools.persistence.HazelCastPersistenceServiceImpl;
import com.akqa.tools.persistence.PersistenceService;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/download", name = "DownloadServlet", asyncSupported = true)
public  class DownloadServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// create a small spreadsheet
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet();

		PersistenceService service = new HazelCastPersistenceServiceImpl();
		Collection<Result> summary = service.getResults(req.getSession().getId()).values();
		int rowIndex = 0;
		for(Result aResult : summary){
			HSSFRow row = sheet.createRow(rowIndex++);
			int columnIndex = 0;
			for(String columnValue : aResult.getAllColumns().values()){
				HSSFCell cell = row.createCell(columnIndex++);
				cell.setCellValue(columnValue);
			}
			HSSFCell cell = row.createCell(columnIndex);
			cell.setCellValue(aResult.getStatus());
		}

		// write it as an excel attachment
		ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
		wb.write(outByteStream);
		byte [] outArray = outByteStream.toByteArray();
		resp.setContentType("application/ms-excel");
		resp.setContentLength(outArray.length);
		resp.setHeader("Expires:", "0"); // eliminates browser caching
		resp.setHeader("Content-Disposition", "attachment; filename=Summary.xls");
		OutputStream outStream = resp.getOutputStream();
		outStream.write(outArray);
		outStream.flush();
	}
}
