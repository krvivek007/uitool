package com.akqa.tools.network;

import com.google.inject.Provider;

public class WorkThreadProvider implements Provider<WorkThread>{

	@Override
	public WorkThread get() {
		return new WorkThread();
	}

}
