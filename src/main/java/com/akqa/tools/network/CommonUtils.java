package com.akqa.tools.network;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.vaadin.ui.UI;

public class CommonUtils {

	public static  <T> Stream<T> iteratorToStream(Iterator<T> itreator){
		return StreamSupport.stream(
				Spliterators.spliteratorUnknownSize(itreator, Spliterator.ORDERED),
				false);
	}
	
	public static String getSessionId(){
		return UI.getCurrent().getSession().getSession().getId();
	}

}
