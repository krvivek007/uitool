package com.akqa.tools.network;

import com.akqa.tools.persistence.PersistanceModule;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class WorkerModule extends AbstractModule {
	@Override
	protected void configure() {
		bindConstant().annotatedWith(Names.named("SessionID")).to(CommonUtils.getSessionId());
		bind(PinServiceControllerImpl.class);
		bind(WorkThread.class).toProvider(WorkThreadProvider.class);
		install(new PersistanceModule());
	}
}
