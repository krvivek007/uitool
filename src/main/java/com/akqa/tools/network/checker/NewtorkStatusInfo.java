package com.akqa.tools.network.checker;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class NewtorkStatusInfo {
	private String url;
	private String ip;
	private String port;
	private List<String> resultCollector;
	
	public void addResult(String result){
		if(resultCollector == null){
			resultCollector = new ArrayList<>();
		}
		resultCollector.add(result);
	}
	
	public String getResult(){
		return resultCollector.stream().collect(Collectors.joining("->"));
	}
	

}
