package com.akqa.tools.network.checker;

import java.net.InetSocketAddress;
import java.net.Socket;

public class PingChecker extends NetworkChecker {

	private static final int MAX_TIME_MILLS = 10000;

	@Override
	protected boolean check(NewtorkStatusInfo row) {
		boolean status = isOnline(row.getUrl(), row.getPort());
		row.addResult(status ? "PORT OPEN" : "PORT CLOSE");
		return status;
	}
	
	public static boolean isOnline(String url, String port) {
	    boolean b = false;
	    try{
	        InetSocketAddress sa = new InetSocketAddress(url, Integer.parseInt(port));
	        Socket ss = new Socket();
	        ss.connect(sa, MAX_TIME_MILLS);
	        ss.close();
	        b = true;
	    }catch(Exception e) {
	        b = false;
	    }
	    return b;
	}
}
