package com.akqa.tools.network.checker;


public abstract class NetworkChecker {
	protected NetworkChecker nextChecker;
	public void setNextChecker(NetworkChecker checker){
		this.nextChecker = checker;
	}

	public void checkAndNext(NewtorkStatusInfo row){
		if(check(row) && nextChecker !=null){
			nextChecker.checkAndNext(row);;
		}
	}
	
	abstract protected boolean check(NewtorkStatusInfo row);

}
