package com.akqa.tools.navigation.service;

import java.util.Arrays;
import java.util.List;

public class ServiceLocatorImpl implements ServiceLocator {

	@Override
	public List<ServiceDetail> getServices() {
		ServiceDetail detail = new ServiceDetail("Network Check",
				"http://localhost:8080/ui#!formview");
		ServiceDetail emailTrigger = new ServiceDetail("Email Trigger",
				"http://localhost:8080/ui#!formview");
		
		ServiceDetail email = new ServiceDetail("Order Email",
				"http://localhost:8080/ui#!formview");
		return Arrays.asList(new ServiceDetail[]{detail, emailTrigger, email});
	}
}
