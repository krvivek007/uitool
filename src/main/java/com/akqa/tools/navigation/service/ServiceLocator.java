package com.akqa.tools.navigation.service;

import java.util.List;

public interface ServiceLocator {
	List<ServiceDetail> getServices();

}
