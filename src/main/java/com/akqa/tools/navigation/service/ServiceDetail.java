package com.akqa.tools.navigation.service;

public class ServiceDetail {
	private final String name;
	private final String endPoint;
	
	public ServiceDetail(String name, String endPoint) {
		this.name = name;
		this.endPoint = endPoint;
	}

	public String getName() {
		return name;
	}

	public String getEndPoint() {
		return endPoint;
	}
}
