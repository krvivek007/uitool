package com.akqa.tools.navigation;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = {"/navigation/*"}, name = "NavigationServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = NavigationUI.class, productionMode = true)
public  class NavigationServlet extends VaadinServlet {
}
