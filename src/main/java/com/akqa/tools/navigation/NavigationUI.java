package com.akqa.tools.navigation;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("mytheme")
public class NavigationUI extends UI {
	Navigator navigator;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		navigator = new Navigator(this, this);
		navigator.addView("navigation", new NavigationUIView());
    	navigator.navigateTo("navigation");
	}
}
