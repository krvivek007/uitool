package com.akqa.tools.navigation;

import java.util.function.Consumer;

import com.akqa.tools.navigation.service.ServiceDetail;
import com.akqa.tools.navigation.service.ServiceLocator;
import com.akqa.tools.navigation.service.ServiceLocatorImpl;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("mytheme")
public class NavigationUIView extends VerticalLayout implements View{
	private  Embedded toolView;
	public NavigationUIView() {
		Responsive.makeResponsive(this);
		setSizeFull();
		HorizontalLayout navigation = getNavigation();
		addComponent(navigation);
		setComponentAlignment(navigation, Alignment.MIDDLE_CENTER);
		setExpandRatio(navigation, 0.1f);
		navigateTo("http://www.montblanc.com/en-us/home.html");
	}

	private void navigateTo(String url){
		if(toolView != null){
			removeComponent(toolView);
		}
		toolView = new Embedded("", new ExternalResource(url));
		toolView.setType(Embedded.TYPE_BROWSER);
		toolView.setWidth("100%");
		toolView.setHeight("100%");
		addComponent(toolView);
		setComponentAlignment(toolView, Alignment.MIDDLE_CENTER);
		setExpandRatio(toolView, 0.8f);
	}

	private HorizontalLayout getNavigation(){
		HorizontalLayout buttons = new HorizontalLayout();
		ServiceLocator locator = new ServiceLocatorImpl();
		locator.getServices().stream().forEach(new Consumer<ServiceDetail>() {
			@Override
			public void accept(ServiceDetail t) {
				Button aToolButton = new Button(t.getName());
				aToolButton.addClickListener(new Button.ClickListener() {
					public void buttonClick(ClickEvent event) {
						navigateTo(t.getEndPoint());
					}
				});
				aToolButton.setStyleName("huge");
				buttons.addComponents(aToolButton);
				buttons.setComponentAlignment(aToolButton, Alignment.MIDDLE_CENTER);
			}

		});
		buttons.setSizeFull();
		return buttons;
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}
}
